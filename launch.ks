//First, we'll clear the terminal screen to make it look nice
CLEARSCREEN.

RUN ONCE fonctions.ks.

PRINT "Making calculations...".
SET ATMO TO SHIP:BODY:ATM.

IF ATMO:EXISTS {
  SET TARGET_ALT TO ATMO:HEIGHT * 10 / 7.
  SET SCALE_HEIGHT TO getScaleHeight(SHIP:BODY).
  LOCK atmP to 2.718^((0-SHIP:ALTITUDE)/SCALE_HEIGHT).  // calulates the Atmospheric pressure // KERBIN ONLY...
  LOCK atmD to atmP * 1.2230948554874.     // calulates the Atmospheric density
} ELSE {
  SET TARGET_ALT TO SHIP:BODY:RADIUS / 5.
}

PRINT "Target altitude : " + ROUND(TARGET_ALT) + ", BODY HAS ATMO : " + ATMO:EXISTS.

//This is our countdown loop, which cycles from 3 to 0
PRINT "Counting down:".
FROM {local countdown is 3.} UNTIL countdown = 0 STEP {SET countdown to countdown - 1.} DO {
    PRINT "..." + countdown.
    WAIT 1. // pauses the script here for 1 second.
}

SAS OFF.

// Calculate gravity and correct it with altitude
SET g TO SHIP:BODY:MU / SHIP:BODY:RADIUS^2.
LOCK galt TO g * (SHIP:BODY:RADIUS / (SHIP:BODY:RADIUS + SHIP:ALTITUDE))^2.

// we start pointing directly up
LOCK curPitch to 90.
LOCK STEERING TO R(0,0,-90) + HEADING(90,curPitch).

SET LASTSECOND TO TIME:SECONDS.
WHEN TIME:SECONDS > LASTSECOND THEN {
  PRINT "Pitch : " + ROUND(curPitch) AT (TERMINAL:WIDTH - 11 , 0).
  IF SHIP:APOAPSIS < TARGET_ALT RETURN True.
}


initialStage().

// Set throttle to get a 1.5 twr if atmo, else 5 should be fine
LOCK twr to (ship:AVAILABLETHRUST+0.01) / (ship:mass *galt).
IF ATMO:EXISTS {
  LOCK THROTTLE TO 1.5 / twr.
} ELSE {
  LOCK THROTTLE TO 5 / twr.
}

WAIT 1.

// Point up at start
WHEN SHIP:VELOCITY:SURFACE:MAG > (g * 5) THEN {
  // Update twr when we leave dense atmosphere
  if ATMO:EXISTS {
    // A essayer :  (1.4^(x/2)-1)*10
    LOCK curPitch TO 90 - MIN((SHIP:ALTITUDE / TARGET_ALT) * 450, LOG10((SHIP:ALTITUDE / TARGET_ALT) * 200 + 1)*35).
    WHEN atmD < 0.205123155 THEN {
      PRINT "Dense atmosphere is behind us, setting throttle to 1.7 TWR...".
      LOCK THROTTLE TO 1.7 / twr.
    }
  } else {
    LOCK curPitch to 90 - LOG10((SHIP:ALTITUDE / TARGET_ALT) * 200 + 1)*35.
  }
}

LIST ENGINES IN engineList.
UNTIL SHIP:APOAPSIS > TARGET_ALT {
  for eng IN engineList {
    if eng:stage >= STAGE:NUMBER AND eng:FLAMEOUT  {
      STAGE.
      PRINT "Staging cause FLAMEOUT.".

      UNTIL STAGE:READY {
        WAIT 0.
      }

      LIST ENGINES IN engineList.
      BREAK.
    }
  }
}

// If we have atmosphere, set SAS to prograde to save some dV minimizing drag forces
IF ATMO:EXISTS {
  UNLOCK ALL.
  SAS ON.
  SET SASMODE TO "PROGRADE".
  WAIT UNTIL SHIP:ALTITUDE > ATMO:HEIGHT.
  SET SASMODE TO "STABILITYASSIST".
}

SET SHIP:CONTROL:PILOTMAINTHROTTLE TO 0.
UNLOCK ALL.
wait 0.1.

// Return the scale height for a body
function getScaleHeight {
  parameter theBody.

  if theBody:NAME = "Kerbin" {
    return 5600.
  } else if theBody:NAME = "Eve" {
    return 7200.
  } else if theBody:NAME = "Duna" {
    return 5700.
  } else if theBody:NAME = "Laythe" {
    return 8000.
  } else if theBody:NAME = "Jool" {
    return 30000.
  }

  return 0.
}

function shouldStage {
  parameter engineListLocal.
  SET STAGESTATUS TO False.
  SET NewList to list().

  IF MAXTHRUST = 0 {
    SET STAGESTATUS TO True.
  }

  for eng IN engineListLocal {
    if eng:FLAMEOUT {
      SET STAGESTATUS TO True.
    } else {
      NewList:ADD(eng).
    }
  }

  SET engineList TO NewList.
  RETURN STAGESTATUS.
}


// Launch staging sequence
// take account of stabillity enchancers
function initialStage {
  list PARTS IN partlist.
  LOCAL hasStabEnhancer IS False.
  LOCAL targetStage IS STAGE:NUMBER - 1.

  for part in partList {
    if part:NAME = "launchClamp1" {
      SET hasStabEnhancer TO True.
      SET targetStage TO part:STAGE.
    }
  }

  if hasStabEnhancer {
    UNTIL STAGE:NUMBER = targetStage {
      STAGE.
      PRINT "Stage to clamp activated.".

      UNTIL STAGE:READY {
        WAIT 0.
      }
    }
  }

  UNTIL SHIP:MAXTHRUST > 0 {
      WAIT 0.5. // pause half a second between stage attempts.
      PRINT "Stage activated.".
      STAGE.
  }
}
